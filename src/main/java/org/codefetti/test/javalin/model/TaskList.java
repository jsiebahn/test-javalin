package org.codefetti.test.javalin.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

public class TaskList {

    @Schema(
            description = "The id is generated internally when creating a new task list.",
            example = "FC3BAD29-C9AD-4245-83D6-427E1441483C"
    )
    private String id;

    @Schema(
            description = "A name for this list.",
            example = "Shopping"
    )
    @NotNull
    @Pattern(regexp = ".+")
    private String name;

    @Schema(
            description = "All tasks in this TaskList"
    )
    private List<Task> tasks = new ArrayList<>();

    public String getId() {
        return id;
    }

    public TaskList setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public TaskList setName(String name) {
        this.name = name;
        return this;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public TaskList setTasks(List<Task> tasks) {
        this.tasks = tasks;
        return this;
    }
}
