package org.codefetti.test.javalin.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class Task {

    @NotNull
    @Pattern(regexp = ".+")
    private String summary;

    private boolean completed;

    public String getSummary() {
        return summary;
    }

    public Task setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public boolean isCompleted() {
        return completed;
    }

    public Task setCompleted(boolean completed) {
        this.completed = completed;
        return this;
    }
}
