package org.codefetti.test.javalin.validation;

import io.javalin.Javalin;
import io.javalin.core.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class JavaX implements Plugin {

    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    public static <T> boolean validate(Object o) {
        Set<ConstraintViolation<Object>> violations = VALIDATOR.validate(o);
        if (violations.isEmpty()) {
            return true;
        }
        throw new JavaX.ValidationException(violations);
    }

    @Override
    public void apply(@NotNull Javalin app) {
        app.exception(ValidationException.class, (exception, ctx) -> {
            ctx.status(422).json(Collections.singletonMap(
                    "errors",
                    exception.getViolations().stream()
                    .map(v -> "" + v.getPropertyPath().toString() + ": " + v.getMessage())
                    .collect(Collectors.toList())
            ));
        });
    }

    private static class ValidationException extends RuntimeException {

        private Set<ConstraintViolation<Object>> violations;

        private ValidationException(Set<ConstraintViolation<Object>> violations) {
            this.violations = violations;
        }

        private Set<ConstraintViolation<Object>> getViolations() {
            return violations;
        }
    }
}
