package org.codefetti.test.javalin.db;

import org.codefetti.test.javalin.model.TaskList;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TaskRepository {

    private Map<String, TaskList> data = new HashMap<>();

    public Collection<TaskList> findAll() {
        return data.values();
    }

    public void save(TaskList taskList) {
        if (taskList.getId() == null) {
            taskList.setId(UUID.randomUUID().toString());
        }
        data.put(taskList.getId(), taskList);
    }

    public TaskList findById(String id) {
        return data.get(id);
    }

    public void deleteById(String id) {
        data.remove(id);
    }
}
