package org.codefetti.test.javalin.config;

import io.javalin.Javalin;

import java.util.Collections;

public class AdminServerFactory {

    public static Javalin startAdminServer(int adminPort, Javalin app) {

        DefaultMetrics metrics = app.config.getPlugin(DefaultMetrics.class);
        Javalin adminServer = Javalin
                .create(config -> {
                    config.showJavalinBanner = false;
                    config.registerPlugin(new RouteLogger());
                    config.registerPlugin(new Env());
                })
                .get("/ping", ctx -> ctx.json(Collections.singletonMap("pong", true)));
        if (metrics != null) {
            adminServer = adminServer.get("/metrics/prometheus", metrics);
        }
        return adminServer.start(adminPort);
    }

}
