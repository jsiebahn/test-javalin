package org.codefetti.test.javalin.config;

import io.javalin.core.JavalinConfig;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.swagger.v3.oas.models.info.Info;
import org.codefetti.test.javalin.validation.JavaX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.Arrays;

public class AppConfigurer {

    public static void configureAppDefaults(JavalinConfig config) {
        config.showJavalinBanner = false;

        config.registerPlugin(new RouteLogger());
        config.registerPlugin(new DefaultMetrics());
        config.registerPlugin(new JavaX());

        initCors(config);
        initRequestLog(config);
    }

    public static void configureOpenApi(JavalinConfig config, Info appInfo) {
        config.registerPlugin(new OpenApiPlugin(createOpenApiOptions(appInfo)));
    }

    private static void initCors(JavalinConfig config) {
        String[] corsOrigins = Env.commaSeparatedArray("CORS_ALLOWED_ORIGINS");
        if (Arrays.asList(corsOrigins).contains("*")) {
            config.enableCorsForAllOrigins();
        }
        else if (corsOrigins.length > 0) {
            config.enableCorsForOrigin(corsOrigins);
        }
    }

    private static void initRequestLog(JavalinConfig config) {
        Logger log = LoggerFactory.getLogger("RequestLogger");
        config.requestLogger(
                (ctx, executionTimeMs) -> log.info(
                        "{} {} {} {} ({}ms)",
                        Instant.now(), ctx.method(), ctx.path(), ctx.status(), executionTimeMs));
    }

    private static OpenApiOptions createOpenApiOptions(Info applicationInfo) {
        return new OpenApiOptions(applicationInfo).path("/swagger.json");
    }

}
