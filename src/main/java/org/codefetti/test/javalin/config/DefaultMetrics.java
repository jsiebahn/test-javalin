package org.codefetti.test.javalin.config;

import io.javalin.Javalin;
import io.javalin.core.plugin.Plugin;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics;
import io.micrometer.core.instrument.binder.jvm.DiskSpaceMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.micrometer.core.instrument.binder.system.UptimeMetrics;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Histogram;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DefaultMetrics implements Plugin, Handler {

    private PrometheusMeterRegistry prometheusRegistry = new PrometheusMeterRegistry(
            PrometheusConfig.DEFAULT);

    @Override
    public void apply(@NotNull Javalin app) {
        Histogram requestDurationHistogram = Histogram
                .build("http_request_duration_seconds", "Tracks the duration of requests")
                .labelNames("httpMethod", "mappedPath", "responseCode")
                .create();
        app.before(ctx -> {
            ctx.attribute("requestStart", System.currentTimeMillis());
        });
        app.after(ctx -> {
            Long requestStart = ctx.attribute("requestStart");
            if (requestStart != null) {
                long durationMillis = System.currentTimeMillis() - requestStart;
                double durationSeconds = ((double) durationMillis) / 1000;
                requestDurationHistogram
                        .labels(ctx.method(), ctx.endpointHandlerPath(), "" + ctx.status())
                        .observe(durationSeconds);
            }
        });
        CollectorRegistry registry = this.prometheusRegistry.getPrometheusRegistry();
        registry.register(requestDurationHistogram);
        new ClassLoaderMetrics().bindTo(this.prometheusRegistry);
        new DiskSpaceMetrics(getTempDir()).bindTo(this.prometheusRegistry);
        new JvmGcMetrics().bindTo(this.prometheusRegistry);
        new JvmMemoryMetrics().bindTo(this.prometheusRegistry);
        new JvmThreadMetrics().bindTo(this.prometheusRegistry);
        new UptimeMetrics().bindTo(this.prometheusRegistry);
        new ProcessorMetrics().bindTo(this.prometheusRegistry);
    }

    @Override
    public void handle(@NotNull Context ctx) {
        ctx.result(prometheusRegistry.scrape());
    }


    private File getTempDir() {
        try {
            return Files.createTempFile("", "").getParent().toFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
