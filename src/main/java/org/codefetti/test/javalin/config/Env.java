package org.codefetti.test.javalin.config;

import io.javalin.Javalin;
import io.javalin.core.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Env implements Plugin {

    private static final Logger LOG = LoggerFactory.getLogger(Env.class);

    private static Set<String> requestedEnvVariables = new LinkedHashSet<>();

    public static String[] commaSeparatedArray(String envName) {
        requestedEnvVariables.add(envName);
        return splitTrimDistinct(defaultValue(
                System.getenv(envName),
                ""
        ));
    }

    public static void log() {
        String collect = requestedEnvVariables
                .stream()
                .sorted()
                .map(e -> e + ": " + (System.getenv(e) == null ? "null" : "*****"))
                .collect(Collectors.joining("\n"));
        LOG.info("Using environment:\n{}", collect);
    }

    @Override
    public void apply(@NotNull Javalin app) {
        app.events(eventListener -> eventListener.serverStarted(Env::log));
    }

    private static  <T> T defaultValue(T configValue, T defaultValue) {
        return configValue != null ? configValue : defaultValue;
    }

    private static String[] splitTrimDistinct(String configValue) {
        return Stream.of(configValue.split(","))
                     .map(String::trim)
                     .filter(s -> s.length() > 0)
                     .distinct()
                     .toArray(String[]::new);
    }
}
