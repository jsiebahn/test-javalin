package org.codefetti.test.javalin.config;

import io.javalin.Javalin;
import io.javalin.core.event.HandlerMetaInfo;
import io.javalin.core.plugin.Plugin;
import io.javalin.http.HandlerType;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparing;

public class RouteLogger implements Plugin {

    private static final Logger LOG = LoggerFactory.getLogger(RouteLogger.class);

    private Javalin javalin;

    private List<HandlerMetaInfo> routes = new ArrayList<>();

    @Override
    public void apply(@NotNull Javalin app) {
        this.javalin = app;
        this.javalin.events(eventListener -> {
            eventListener.handlerAdded(this::handlerAdded);
            eventListener.serverStarted(this::serverStarted);
        });
    }

    private void handlerAdded(HandlerMetaInfo handlerMetaInfo) {
        routes.add(handlerMetaInfo);
    }

    private void serverStarted() {
        routes.stream()
              .filter(r -> r.getHttpMethod() != HandlerType.BEFORE)
              .filter(r -> r.getHttpMethod() != HandlerType.AFTER)
              .sorted(
                      comparing(HandlerMetaInfo::getPath)
                              .thenComparing(HandlerMetaInfo::getHttpMethod)
              )
              .forEach(r -> LOG.info("Route http://localhost:{} {} {}",
                                     javalin.port(),
                                     r.getHttpMethod(),
                                     r.getPath())
              );
    }
}
