package org.codefetti.test.javalin;

import io.javalin.Javalin;
import io.swagger.v3.oas.models.info.Info;
import org.codefetti.test.javalin.config.AdminServerFactory;
import org.codefetti.test.javalin.config.AppConfigurer;
import org.codefetti.test.javalin.db.TaskRepository;
import org.codefetti.test.javalin.rest.TaskListController;

import static io.javalin.apibuilder.ApiBuilder.path;

public class Application {

    private final int appPort;
    private final int adminPort;

    private Javalin app;
    private Javalin admin;


    public Application(int appPort, int adminPort) {
        this.appPort = appPort;
        this.adminPort = adminPort;
    }

    public static void main(String[] args) {
        new Application(8080, 8081).bootstrap();
    }

    public Application bootstrap() {

        TaskListController taskListController = new TaskListController(new TaskRepository());

        app = Javalin
                .create(config -> {
                    AppConfigurer.configureAppDefaults(config);
                    AppConfigurer.configureOpenApi(config, new Info()
                            .version("1")
                            .title("TaskList")
                            .description("A Task List Application")
                    );
                })
                .routes(() -> path("/api", () -> {
                    //noinspection Convert2MethodRef
                    taskListController.endpoints();
                }))
                .start(appPort);

        admin = AdminServerFactory.startAdminServer(adminPort, app);

        return this;
    }


    // to control application in unit tests

    public void stop() {
        app.stop();
        admin.stop();
    }

    public Javalin getApp() {
        return app;
    }

    public Javalin getAdmin() {
        return admin;
    }
}
