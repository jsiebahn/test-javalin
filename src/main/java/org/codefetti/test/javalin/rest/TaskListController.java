package org.codefetti.test.javalin.rest;

import io.javalin.http.Context;
import org.codefetti.test.javalin.db.TaskRepository;
import org.codefetti.test.javalin.model.Task;
import org.codefetti.test.javalin.model.TaskList;
import org.codefetti.test.javalin.validation.JavaX;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.plugin.openapi.dsl.OpenApiBuilder.document;
import static io.javalin.plugin.openapi.dsl.OpenApiBuilder.documented;

public class TaskListController {

    private TaskRepository taskRepository;

    public TaskListController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void endpoints() {
        path("/taskLists", () -> {
            get(documented(
                    document()
                            .operation(operation -> {
                                operation.description("Finds all task lists.");
                            })
                            .jsonArray("200", TaskList.class),
                    this::getAll
                )
            );
            post(documented(
                    document()
                            .operation(operation -> {
                                operation.description("Adds a new task list.");
                            })
                            .body(TaskList.class)
                            .result("201"),
                    this::newTaskList
                 )
            );
            path("/:id", () -> {
                get(this::getOne);
                delete(this::deleteTaskList);
                path("/tasks", () -> {
                    post(documented(
                            document()
                                    .operation(operation -> {
                                        operation.description("Adds a new task to the list.");
                                    })
                                    .body(Task.class)
                                    .result("201"),
                            this::addTaskToList
                    ));
                });
            });
        });
    }

    public void getAll(Context ctx) {
        ctx.json(taskRepository.findAll());
    }

    public void newTaskList(Context ctx) {
        TaskList taskList = ctx.bodyValidator(TaskList.class).check(JavaX::validate).get();
        taskRepository.save(taskList);
        ctx.header("Location", ctx.fullUrl() + "/" + taskList.getId()).status(201);
    }

    public void deleteTaskList(Context ctx) {
        taskRepository.deleteById(ctx.pathParam("id"));
    }

    public void getOne(Context ctx) {
        TaskList taskList = taskRepository.findById(ctx.pathParam("id"));
        if (taskList == null) {
            ctx.status(404);
        }
        else {
            ctx.json(taskList);
        }
    }

    public void addTaskToList(Context ctx) {
        TaskList taskList = taskRepository.findById(ctx.pathParam("id"));
        if (taskList == null) {
            ctx.status(404);
        }
        else {
            String actionUrl = ctx.fullUrl();
            taskList.getTasks().add(ctx.bodyValidator(Task.class).check(JavaX::validate).get());
            ctx.redirect(actionUrl.substring(0, actionUrl.lastIndexOf("/")), 303);
        }
    }
}
