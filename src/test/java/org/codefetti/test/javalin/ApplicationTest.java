package org.codefetti.test.javalin;


import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static io.javalin.plugin.json.JavalinJackson.getObjectMapper;
import static org.apache.http.client.fluent.Request.Get;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class ApplicationTest {

    private static Application application;

    @BeforeClass
    public static void setUp() {
        application = new Application(0,0).bootstrap();
    }

    @AfterClass
    public static void tearDown() {
        application.stop();
    }

    @Test
    public void supportPingAtAdminPort() throws IOException {

        HttpResponse response = Get(adminPath("/ping"))
                .setHeader("Accept", "application/json")
                .execute()
                .returnResponse();

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
        Map<String, Object> result = getObjectMapper().readValue(
                response.getEntity().getContent(),
                new TypeReference<Map<String, Object>>() {
        });
        assertThat(result).containsExactly(entry("pong", true));

    }

    @Test
    public void provideMetricsForApp() throws IOException {
        Get(appPath("/api/taskLists"))
                .setHeader("Accept", "application/json")
                .execute();

        Get(appPath("/api/taskLists/D9CFB57F-ED8D-4ED6-BB67-B983D279889C"))
                .setHeader("Accept", "application/json")
                .execute();

        HttpResponse response = Get(adminPath("/metrics/prometheus"))
                .setHeader("Accept", "application/json")
                .execute()
                .returnResponse();

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
        try (InputStream is = response.getEntity().getContent()) {
            String content = IOUtils.toString(is, StandardCharsets.UTF_8);
            assertThat(content)
                    .contains("http_request_duration_seconds_count")
                    .contains("http_request_duration_seconds_sum")
                    .contains("http_request_duration_seconds_bucket")
                    .contains("http_request_duration_seconds_count{httpMethod=\"GET\"," +
                                      "mappedPath=\"/api/taskLists/:id\",responseCode=\"404\",}")
                    .contains("http_request_duration_seconds_count{httpMethod=\"GET\"," +
                                      "mappedPath=\"/api/taskLists\",responseCode=\"200\",}")
            ;
        }
    }

    private String appPath(String path) {
        return appBaseUrl() + path;
    }

    private String appBaseUrl() {
        return "http://localhost:" + application.getApp().port();
    }

    private String adminPath(String path) {
        return adminBaseUrl() + path;
    }

    private String adminBaseUrl() {
        return "http://localhost:" + application.getAdmin().port();
    }
}
