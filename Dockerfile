FROM openjdk:8-jre-alpine

MAINTAINER Jörg Siebahn <joerg@codefetti.org>

COPY build/install/test-javalin/bin/* /test-javalin/bin/
COPY build/install/test-javalin/lib/* /test-javalin/lib/

ENTRYPOINT ["/test-javalin/bin/test-javalin"]
